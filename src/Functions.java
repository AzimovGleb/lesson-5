import java.util.Collections;
import java.util.List;

public class Functions {
    public static List<Animal> sortOne(List<Animal> list) {
        Collections.sort(list, Animal.COMPARE_BY_WEIGHT);
        return list;
    }

    public static List<Animal> sortReverse(List<Animal> list) {
        Collections.sort(list, Collections.reverseOrder(Animal.COMPARE_BY_WEIGHT));
        return list;
    }

    public static Object findbyWeight(List<Animal> list, double findElement) {
        for (Animal m: list) {
            if (findElement == m.getWeight()) return m;
        }
        return "Not find element";
    }

    public static Object findbyNickname(List<Animal> list, String findElement) {
        for (Animal m: list) {
            if (findElement.equals(m.getNickname())) return m;
        }
        return "Not find element";
    }

    public static Object findbyOld(List<Animal> list, double findElement) {
        for (Animal m: list) {
            if (findElement == m.getOld()) return m;
        }
        return "Not find element";
    }
}
