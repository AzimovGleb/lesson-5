
public class Mammal extends Animal{

    private String gender;

    public Mammal(String kind, long weight, String nickname, int popular, double speed, int old, String gender) {
        super(kind, weight, nickname, popular, speed, old);
        this.gender = gender;
    }
}
