public class Cheetah extends Mammal {


    private int numberOfSpots;
    private double temperatureAtMaximumSpeed;

    {
        numberOfSpots = 79;
        temperatureAtMaximumSpeed = 42;
    }


    public Cheetah(String kind, long weight, String nickname, int popular, double speed, int old, String gender) {
        super(kind, weight, nickname, popular, speed, old, gender);
    }
}
