
public class Eagle extends Bird {

    private double viewingRange;

    {
        viewingRange = 3000;
    }

    public Eagle(String kind, long weight, String nickname, int popular, double speed, int old, double wingspan) {
        super(kind, weight, nickname, popular, speed, old, wingspan);

    }

    public double getViewingRange() {
        return viewingRange;
    }

    public void setViewingRange(double viewingRange) {
        this.viewingRange = viewingRange;
    }
}
