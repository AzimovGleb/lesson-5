import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Animal implements Comparator<Animal> {

    private String kind;
    private long weight;
    private String nickname;
    private int popular;
    private double speed;
    private int old;

    public Animal(String kind, long weight, String nickname, int popular, double speed, int old) {
        this.kind = kind;
        this.weight = weight;
        this.nickname = nickname;
        this.popular = popular;
        this.speed = speed;
        this.old = old;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getPopular() {
        return popular;
    }

    public void setPopular(int popular) {
        this.popular = popular;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getOld() {
        return old;
    }

    public void setOld(int old) {
        this.old = old;
    }

    @Override
    public int compare(Animal o1, Animal o2) {
        return Math.toIntExact((o1.getWeight() - o2.getWeight()));
    }

    @Override
    public String toString() {
        return "Animal{" +
                "kind='" + kind + '\'' +
                ", weight=" + weight +
                ", nickname='" + nickname + '\'' +
                ", popular=" + popular +
                ", speed=" + speed +
                ", old=" + old +
                '}';
    }

    public static final Comparator<Animal> COMPARE_BY_WEIGHT = new Comparator<Animal>() {
        @Override
        public int compare(Animal lhs, Animal rhs) {
            return Math.toIntExact(lhs.getWeight() - rhs.getWeight());
        }
    };
}
