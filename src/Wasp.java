public class Wasp extends Animal{

   private int poisonPower;
   private int numberOfStripes;

    {
        poisonPower = 5;
        numberOfStripes = 21;
    }


    public Wasp(String kind, long weight, String nickname, int popular, double speed, int old) {
        super(kind, weight, nickname, popular, speed, old);
    }

}
