public class Owl extends Bird {

    private double degreeOfRotationOfTheHead;

    {
        degreeOfRotationOfTheHead = 280;
    }

    public Owl(String kind, long weight, String nickname, int popular, double speed, int old, double wingspan) {
        super(kind, weight, nickname, popular, speed, old, wingspan);
    }
}
