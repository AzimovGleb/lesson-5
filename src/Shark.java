public class Shark extends Animal {

    private int numberOfGills;
    private int numberOfTeeth;

    {
        numberOfTeeth = 1000;
        numberOfGills = 5;
    }

    public Shark(String kind, long weight, String nickname, int popular, double speed, int old) {
        super(kind, weight, nickname, popular, speed, old);
    }
}
