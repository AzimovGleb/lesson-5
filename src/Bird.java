public class Bird extends Animal {

    private double wingspan;

    public Bird(String kind, long weight, String nickname, int popular, double speed, int old, double wingspan) {
        super(kind, weight, nickname, popular, speed, old);
        this.wingspan = wingspan;
    }

    public double getWingspan() {
        return wingspan;
    }

    public void setWingspan(double wingspan) {
        this.wingspan = wingspan;
    }
}
