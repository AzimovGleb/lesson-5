import java.util.*;

public class Main {

    public static void main(String[] args) {
        Functions functions = new Functions();
        List<Animal> zooWeightList = new ArrayList<>();
        Dog dog = new Dog("Mammel", 8000, "Racks", 8, 40, 6, "male");
        Cat cat = new Cat("Mammel", 1150, "Том", 9, 20, 6, "female");
        Wasp wasp = new Wasp("Insecta", 2, "Maya", 6, 10, 1);
        Ant ant = new Ant("Insecta", 1, "Walley", 3, 5, 2);
        Tuna tuna = new Tuna("Fish", 2000, "Bernard", 2, 15, 3);
        Shark shark = new Shark("Fish", 500000, "Tigerfish", 1, 32, 16);
        Cheetah cheetah = new Cheetah("Mammel", 45000, "Spaick", 4, 182, 10, "male");

        zooWeightList.add(dog);
        zooWeightList.add(cat);
        zooWeightList.add(wasp);
        zooWeightList.add(ant);
        zooWeightList.add(tuna);
        zooWeightList.add(shark);
        zooWeightList.add(cheetah);

        functions.sortReverse(zooWeightList);
        System.out.println(functions.findbyWeight(zooWeightList, dog.getWeight()) + "\n");

        System.out.println(functions.findbyNickname(zooWeightList, cat.getNickname()) + "\n");

        System.out.println(functions.findbyOld(zooWeightList, wasp.getOld()) + "\n");

        for (Animal m : zooWeightList
        ) {
            System.out.println(m);
        }
    }
}
