public class Cat extends Mammal {


    private double highJump;
    private int numberFotos;

    {
        highJump = 1.5;
        numberFotos = 300;
    }


    public Cat(String kind, long weight, String nickname, int popular, double speed, int old, String gender) {
        super(kind, weight, nickname, popular, speed, old, gender);
    }
}
