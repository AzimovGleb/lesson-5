
public class BlueWhale extends Mammal {

   private double bodyLength;

    {
        bodyLength = 30;
    }


    public BlueWhale(String kind, long weight, String nickname, int popular, double speed, int old, String gender) {
        super(kind, weight, nickname, popular, speed, old, gender);
    }
}
