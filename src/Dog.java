
public class Dog extends Mammal {

    private int rage;

    {
        rage = 7;
    }


    public Dog(String kind, long weight, String nickname, int popular, double speed, int old, String gender) {
        super(kind, weight, nickname, popular, speed, old, gender);
    }
}
