public class Ant extends Animal {

    private int eggProduction;
    private int antStrength;

    {
        eggProduction = 20;
        antStrength = 10;
    }

    public Ant(String kind, long weight, String nickname, int popular, double speed, int old) {
        super(kind, weight, nickname, popular, speed, old);
    }
}
