
public class Tuna extends Animal {

    private double maximumHabitatDepth;
    private double minimumHabitatTemperature;

    {
        maximumHabitatDepth = 985;
        minimumHabitatTemperature = 5;
    }

    public Tuna(String kind, long weight, String nickname, int popular, double speed, int old) {
        super(kind, weight, nickname, popular, speed, old);
    }
}
